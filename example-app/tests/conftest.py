from atila.pytest_hooks import *
from rs4 import partial, importer
from skitai.testutil import launcher
from rs4.webkit import webtest
import pytest
import os
import sys
import functools
import time
from functools import wraps
from skitai.testutil import offline
import skitai
from websocket import create_connection
sys.path.append (os.path.join (os.path.dirname (__file__), '../../notebooks'))

PORT = 5000

# test clients -----------------------------------
@pytest.fixture
def _client ():
    launch = partial (launcher.Launcher, port = PORT, ssl = False, silent = True, dry = True)
    client_ = launch ("../../skitaid.py")
    yield client_
    client_.stop ()

@pytest.fixture
def requests (_client):
    return _client.http

@pytest.fixture
def axios (_client):
    return _client.axios

@pytest.fixture
def siesta (_client):
    return _client.siesta
