import re
import sys
import os
import glob

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

with open('ecsdep/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*"(.*?)"',fd.read(), re.M).group(1)

if 'publish' in sys.argv:
    os.system ('{} setup.py bdist_wheel'.format (sys.executable))
    whl = glob.glob ('dist/ecsdep-{}-*.whl'.format (version))[0]
    os.system ('twine upload {}'.format (whl))
    os.system ('pip3 install -U --pre ecsdep==1000 > /dev/null 2>&1')
    sys.exit ()

classifiers = [
  'License :: OSI Approved :: MIT License',
  'Development Status :: 3 - Alpha',
  'Topic :: Utilities',
    'Environment :: Console',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Intended Audience :: Developers',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3'
]

packages = [
    'ecsdep',
]
package_dir = {
    'ecsdep': 'ecsdep'
}
package_data = {
    "ecsdep": [
        "templates/*.json",
        "templates/ecs-cluster/policies/*.json",
        "templates/ecs-cluster/*.tf*",
        "templates/task-def/*.tf*"
    ]
}

install_requires = [
    "click",
    "PyYAML"
]

if __name__ == "__main__":
    with open ('README.md', encoding='utf-8') as f:
        long_description = f.read()

    setup(
        name='ecsdep',
        entry_points={
            'console_scripts': [
                'ecsdep = ecsdep.ecsdep:main',
            ],
        },
        version=version,
        description='AWS ECS Deployment Tool With Terraform',
        long_description = long_description,
        long_description_content_type = 'text/markdown',
        url = 'https://gitlab.com/skitai/ecsdep',
        author='Hans Roh',
        author_email='hansroh@gmail.com',
        packages=packages,
        package_dir=package_dir,
        package_data = package_data,
        license='MIT',
        platforms = ["posix"],
        download_url = "https://pypi.python.org/pypi/ecsdep",
        install_requires = install_requires,
        classifiers=classifiers
    )
