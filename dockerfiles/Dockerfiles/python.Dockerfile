ARG PYVERSION=3.11

FROM python:${PYVERSION}-slim

ARG DEBIAN_FRONTEND=noninteractive
ENV PYTHONUNBUFFERED=1

LABEL title="Base Image"
LABEL author="hansroh"
LABEL version="1.0"

WORKDIR /
ENV LC_ALL=C.UTF-8

COPY scripts/init.sh /init.sh
RUN /bin/bash /init.sh && rm -f /init.sh
RUN pip3 install -U pip

CMD [ "/bin/bash" ]
