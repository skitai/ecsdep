#! /bin/bash
set -e

docker build -t hansroh/ecsdep:24 --build-arg DOCKER_TAG=24 --target image-latest -f Dockerfiles/ecsdep.Dockerfile .
docker build -t hansroh/ecsdep:24-dind --build-arg DOCKER_TAG=24-dind --target image-dind -f Dockerfiles/ecsdep.Dockerfile .
docker build -t hansroh/python:3.11 --build-arg PYVERSION=3.11 -f Dockerfiles/python.Dockerfile .
docker build -t hansroh/tensorflow:2.6-gpu -f Dockerfiles/tensorflow.Dockerfile .

if [ "$1" == "--push" ]
then
    docker push hansroh/ecsdep:24
    docker push hansroh/ecsdep:24-dind
    docker push hansroh/python:3.11
    docker push hansroh/tensorflow:2.6-gpu
fi
