#### Creating Development Dockers
```shell
cd ~/projects/ecsdep
docker-compose up -d
```

#### Testing `app` and `ecsdep`
```shell
docker exec -it ecsdep bash
pip3 install -e .
./init-container.sh
cd tests && ./test-all.sh
```

#### Login Gitlab Registry
Gitlab personal access token must have these permissions:
- api
- read registry
- write registry

```shell
docker login -u <username> -p <personal access token> registry.gitlab.com
```

#### Testing Deployment
```shell
cd tests
./test_example_app.sh
./test_deploy.sh
./test_destroy.sh --cluster
```
